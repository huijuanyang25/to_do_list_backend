USE student_manage_sys_yanghuijuan;

CREATE TABLE todoitems (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`content` VARCHAR(100) NOT NULL,
`status` VARCHAR(100) NOT NULL
)ENGINE = InnoDB DEFAULT CHARSET = utf8;

ALTER TABLE todoitems CHANGE content text VARCHAR(100) NOT NULL;
Select * FROM todoitems;