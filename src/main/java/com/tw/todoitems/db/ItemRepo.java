package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        // Need to be implemented
        String querySQL = "INSERT INTO todoitems(text, status) VALUES (\"" + item.getText() + "\", \"" + item.getStatus() + "\");";
        String itemId = "SELECT id FROM todoitems WHERE text = \"" + item.getText() + "\";";
        try (
                Connection con = connector.createConnect();
                Statement statement = con.createStatement();
                ) {
            statement.execute(querySQL);
            ResultSet resultSet = statement.executeQuery(itemId);
            resultSet.next();
            item.setId(resultSet.getInt(1));
            return item;
        } catch (SQLException e) {
            System.out.print("Add items failed!");
        }
        return null;
    }

    public List<Item> findItems() {
        // Need to be implemented
        List<Item> foundItemsList = new ArrayList<>();
        String querySQL = "SELECT * FROM todoitems;";
        try (
                Connection con = connector.createConnect();
                Statement statement = con.createStatement();
                ) {
            ResultSet resultSet = statement.executeQuery(querySQL);
            while (resultSet.next()) {
                Item foundItems = new Item(resultSet.getInt(1),
                        resultSet.getString(2),
                        ItemStatus.valueOf(resultSet.getString(3)));
                foundItemsList.add(foundItems);
            }
        } catch (SQLException e) {
            System.out.println("Find items failed!");
        }

        return foundItemsList;
    }

    public boolean updateItem(Item item) {
        // Need to be implemented
        String querySQL = "UPDATE todoitems SET text = \"" + item.getText() + "\", status = \""
                + item.getStatus() + "\" WHERE id = " + item.getId() + ";";
        int updateResult = 0;
        try (
                Connection con = connector.createConnect();
                Statement statement = con.createStatement();
                ) {
            updateResult = statement.executeUpdate(querySQL);
        } catch (SQLException e) {
            System.out.println("Update item failed!");
        }

        return updateResult > 0 ? true : false;
    }

    public boolean deleteItem(int id) {
        // Need to be implemented
        String querySQL = "DELETE FROM todoitems WHERE id = " + id + ";";
        int deleteResult = 0;
        try (
                Connection con = connector.createConnect();
                Statement statement = con.createStatement();
                ) {
            deleteResult = statement.executeUpdate(querySQL);
        } catch (SQLException e) {
            System.out.println("Delete items failed!");
        }

        return deleteResult > 0 ? true : false;
    }

}
